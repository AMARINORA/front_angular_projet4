export const environment = {
  production: true,
  PROJET4_AL7_API: "https://projet4-nodejs.herokuapp.com",
  PROJET4_AL7_API_JAVA: "https://projet4-spring-java.herokuapp.com"
};
