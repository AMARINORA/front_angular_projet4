export const environment = {
    production: true,
    PROJET4_AL7_API: "https://projet4-nodejs-staging.herokuapp.com",
    PROJET4_AL7_API_JAVA: "https://projet4-spring-java-staging.herokuapp.com"
  };
  