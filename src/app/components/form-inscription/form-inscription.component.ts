import { Component, Input, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Adresse } from 'src/app/Models/adresse';
import { Utilisateur } from 'src/app/Models/utilisateur';

import { UtilisateurService } from '../../services/utilisateur.service';

@Component({
  selector: 'app-form-inscription',
  templateUrl: './form-inscription.component.html',
  styleUrls: ['./form-inscription.component.css']
})
export class FormInscriptionComponent implements OnInit {

  adresse: Adresse = {
    numeroRue: '',
    rue: '',
    codePostale: '',
    ville: '',
  }
  utilisateur: Utilisateur = {
    nom: '',
    prenom: '',
    adresse: this.adresse,
    mail: '',
    pwd: '',
  }
  message: "Votre compte a bien été créé !";
  isInscriptionFailed =false;
  errorMessage;

  constructor(private _utilisateurService: UtilisateurService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    const data = {
      nom: this.utilisateur.nom,
      prenom: this.utilisateur.prenom,
      numeroRue: this.utilisateur.adresse.numeroRue,
      rue: this.utilisateur.adresse.rue,
      codePostale: this.utilisateur.adresse.codePostale,
      ville: this.utilisateur.adresse.ville,
      mail: this.utilisateur.mail,
      pwd: this.utilisateur.pwd
    };

    this._utilisateurService.AddUser(data).subscribe(
      response => {
        console.log(response)
        this.isInscriptionFailed = false; 
      });
    err => {
      console.log(err.error.message)
      // this.errorMessage = err.error.message;
      this.errorMessage = "Veuillez vérifier votre saisie !"
      this.isInscriptionFailed = true;
    }
  };





}
