import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from 'src/app/Models/utilisateur';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-bord-user',
  templateUrl: './bord-user.component.html',
  styleUrls: ['./bord-user.component.css']
})
export class BordUserComponent implements OnInit {

  content?: string;
  user;
  isUser = false;

  constructor(private tokenStorageService : TokenStorageService, private router: Router) { }

  ngOnInit(): void {
    this.user = this.tokenStorageService.getUser();
    if(this.user.role == "User")
    this.isUser = true;
    // this.userService.getUserBoard().subscribe(
    //   data => {
    //     this.content = data;
    //   },
    //   err => {
    //     this.content = JSON.parse(err.error).message;
    //   }
    // );
  }

  onDeconnect(): void {
    this.tokenStorageService.signOut();
    this.router.navigate(['/login'])
  }

}
