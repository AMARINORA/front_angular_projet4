import { Component, OnInit } from '@angular/core';
import { SortiesService } from 'src/app/services/sorties.service';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.css']
})
export class RechercheComponent implements OnInit {

 sorties;
 arrdt;
 categorie;
 typePrix;
 blind = 1;
 deaf= 1;
 gratuit;
 payant;
 conso;

  constructor(private _sortiesService: SortiesService) { }

  ngOnInit(): void {
  }

  selectChangeHandler (event: any ){
    this.typePrix = event.target.value
  }

  rechercherSortiesByArrdt(): void {
    this._sortiesService.getSortiesByArrdt(this.arrdt)
      .subscribe(data => {this.sorties = data;
        console.log(this.sorties)
        for (let sortie of this.sorties)
        sortie.fields.description = sortie.fields.description.replace(/<[^>]*>?/gm, '')
        },      
        error => { console.log(error);
        });
  }

  rechercherSortiesByCategorie(): void {
    this._sortiesService.getSortiesByCategorie(this.categorie)
      .subscribe(data => {this.sorties = data;
        console.log(this.sorties)
        for (let sortie of this.sorties)
        sortie.fields.description = sortie.fields.description.replace(/<[^>]*>?/gm, '')
        },      
        error => { console.log(error);
        });
  }

  // typePrixList: any = ['Gratuit', 'Payant', 'Conso']

  rechercherSortiesByTypePrix(): void {
    this._sortiesService.getSortiesByPrixType(this.typePrix)
    
      .subscribe(data => {this.sorties = data;
        console.log(this.sorties)
        for (let sortie of this.sorties)
        sortie.fields.description = sortie.fields.description.replace(/<[^>]*>?/gm, '')
        },      
        error => { console.log(error);
        });
  }

  rechercherSortiesPourAveugles(): void {
    this._sortiesService.getSortiesForBlind()
      .subscribe(data => {this.sorties = data;
        console.log(this.sorties)
        for (let sortie of this.sorties)
        sortie.fields.description = sortie.fields.description.replace(/<[^>]*>?/gm, '')
        },      
        error => { console.log(error);
        });
  }

  rechercherSortiesPourMalentendants(): void {
    this._sortiesService.getSortiesForDeaf()
      .subscribe(data => {this.sorties = data;
        console.log(this.sorties)
        for (let sortie of this.sorties)
        sortie.fields.description = sortie.fields.description.replace(/<[^>]*>?/gm, '')
        },      
        error => { console.log(error);
        });
  }

  

  

  

}
