import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Sortie } from 'src/app/Models/sortie';
import { FavoriService } from 'src/app/services/favori.service';
import { SortiesService } from 'src/app/services/sorties.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-sorties',
  templateUrl: './sorties.component.html',
  styleUrls: ['./sorties.component.css']
})
export class SortiesComponent implements OnInit {

  sortieCourante : Sortie;
  indexCourant : number;
  sorties;
  arrdt: any;
  favori;
  user;
  isConnecte = false;
  ajouterFavoris= true;

  constructor(private _sortiesService: SortiesService, private _favoriService: FavoriService, private router: Router, private route: ActivatedRoute, 
    private tokenStorageService: TokenStorageService,private _utilisateurService: UtilisateurService,) { }

  ngOnInit(): void {
    this.getSorties();
    this.user = this.tokenStorageService.getUser()
    if(this.user.role == "Admin")
    this.ajouterFavoris = false
  }

  getSorties() : void{
    const dataUser = {
      utilisateur: this.tokenStorageService.getUser()
    }; 
    this._sortiesService.getToutesLesSorties()
      .subscribe(data => {this.sorties = data;
        console.log(this.sorties)
        for (let sortie of this.sorties)  {
          if (dataUser.utilisateur != null){
            this.isConnecte = true;
            this._utilisateurService.getIsFavoris(sortie._id,dataUser.utilisateur.id)
            .subscribe(res => {
              console.log(res)
              sortie.isFavoris = res
              })          
          }      
        sortie.fields.description = sortie.fields.description.replace(/<[^>]*>?/gm, '')
        }     
        },      
        error => { console.log(error);
        });
  }

  searchSortiesByArrdt(): void {
    this._sortiesService.getSortiesByArrdt(this.arrdt)
      .subscribe(data => {this.sorties = data;
        console.log(this.sorties)
        for (let sortie of this.sorties){
          sortie.fields.description = sortie.fields.description.replace(/<[^>]*>?/gm, '')
        }
        },      
        error => { console.log(error);
        });
  }

  setActiveSortie(sortie: Sortie, index: number): void {
    this.sortieCourante = sortie;
    console.log(this.sortieCourante)
    this.indexCourant = index;
  }

  ajouterFavori(idSortie): void {
    const data = {
      utilisateur: this.tokenStorageService.getUser(),
      //idSortie: this.sortieCourante._id, 
      idSortie:idSortie
    };
    if (data.utilisateur == null){
      this.router.navigate(['/login']);
    } else
    this._favoriService.AddFavori(data)
     .subscribe(response => {
      this.getSorties();
      console.log(response)
    })    
  }
 
  searchFavori(): void {
    this.user = this.tokenStorageService.getUser()
    const data = {
      idUtilisateur: this.user.id,
      idSortie: this.sortieCourante._id, 
    };
    console.log(data)
    this._favoriService.chercherFavori(data)
      .subscribe(data => {this.favori = data;
        console.log(this.favori)
        },      
        error => { console.log(error);
        });
  }

  retirerFavori(idSortie: any): void {
    this.user = this.tokenStorageService.getUser()
    console.log(this.user.id)
    this._favoriService.deleteFavori(idSortie,this.user.id)
      .subscribe(response => {
          console.log(response);
          // this.router.navigate(['/login']);
          this.getSorties();
        },
        error => {
          console.log(error);
        });
  }


}
