import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from 'src/app/Models/utilisateur';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {

  content?: string;
  user;
  isAdmin = false;
  utilisateurs;
  utilisateurCourant? : Utilisateur;
  nom : '';
  prenom : '';
  mail : '';
  indexCourant: number;

  constructor(private tokenStorageService: TokenStorageService, private _utilisateurService: UtilisateurService, 
    private router: Router, private _location: Location) { }

  ngOnInit(): void {
    this.user = this.tokenStorageService.getUser()
    console.log(this.user)
    if(this.user.role == "Admin")
    this.isAdmin = true;
    this.getUtilisateurs();
  }

  backClicked() {
    this._location.back();
  }

  onDeconnect(): void {
    this.tokenStorageService.signOut();
    this.router.navigate(['/login'])
  }

  getUtilisateur(id: string): void {
    this._utilisateurService.getUserById(id)
      .subscribe(data => {
          this.user = data;
          console.log(data)
        },
        error => {
          console.log(error);
        });
  }

  getUtilisateurs(): void {
    this._utilisateurService.getUsers()
      .subscribe(data => {this.utilisateurs = data;
        },      
        error => { console.log(error);
        });
  }

  setActiveUtilisateur(utilisateur: Utilisateur, index: number): void {
    this.utilisateurCourant = utilisateur;
    console.log(this.utilisateurCourant)
    this.indexCourant = index;
  }

  getUtilisateurByMail(): void {
    this._utilisateurService.getUserByMail(this.mail)
      .subscribe( data => {
          this.utilisateurs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  getUtilisateurByNomPrenom(): void {
    this._utilisateurService.getUserByNomEtPrenom(this.nom, this.prenom)
      .subscribe( data => {
          this.utilisateurs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.getUtilisateurs();
    this.utilisateurCourant = undefined;
    this.indexCourant = -1;
  }

  deleteUtilisateur(idUtilisateur): void {
    this._utilisateurService.deleteUser(idUtilisateur)
      .subscribe(response => {
          console.log(response);
         this.refreshList();
         this.router.navigate(['/dashboard-admin']);
        //  this.getUtilisateurs();
        },
        error => {
          console.log(error);
        });
  }

  rechercherUtilisateurNomPrenom(): void {
    this._utilisateurService.getUserByNomEtPrenom(this.nom, this.prenom)
      .subscribe(data => {this.utilisateurs = data;
        console.log(this.utilisateurs)
        },      
        error => { console.log(error);
        });
  }

}
