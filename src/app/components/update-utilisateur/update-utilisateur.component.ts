import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Adresse } from 'src/app/Models/adresse';
import { Utilisateur } from 'src/app/Models/utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import {Location} from '@angular/common';
import { TokenStorageService } from 'src/app/services/token-storage.service';


@Component({
  selector: 'app-update-utilisateur',
  templateUrl: './update-utilisateur.component.html',
  styleUrls: ['./update-utilisateur.component.css']
})
export class UpdateUtilisateurComponent implements OnInit {
  utilisateurs? : Utilisateur[];
  user;
  // test;

  adresseCourante : Adresse = {
    numeroRue : '',
    rue : '',
    codePostale : '',
    ville : '',
  }

  utilisateurCourant : Utilisateur = {
    nom : '',
    prenom : '',
    adresse: this.adresseCourante,
    mail : '',
    pwd : '',
  }
  message = '';
  utilisateur;
  modifierPwd = true;
  isModificationFailed= false;
  errorMessage = '';


  constructor(private _utilisateurService: UtilisateurService, private route: ActivatedRoute, private _location: Location, 
    private tokenStorageService: TokenStorageService, private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.user = this.getUtilisateur(this.route.snapshot.params.id);
    this.utilisateur = this.tokenStorageService.getUser()
    console.log(this.utilisateur)
    if(this.utilisateur.role == "Admin")
      this.modifierPwd = false;   
  }

  backClicked() {
    this._location.back();
  }

  getUtilisateur(id: string): void {
    this._utilisateurService.getUserById(id)
      .subscribe(data => {
          this.user = data;
          // console.log(data)
        },
        error => {
          console.log(error);
        });
  }
  
  updateUtilisateur(): void {
    this._utilisateurService.updateUser(this.user.id, this.user)
      .subscribe(data => {
          console.log(data);
          console.log(this.user)
          this.isModificationFailed = false;
          this.backClicked()
           
        },
        error => {
          console.log(error);
          this.errorMessage = "Veuillez vérifier votre saisie !"
          this.isModificationFailed = true;
        });
  }

  

}
