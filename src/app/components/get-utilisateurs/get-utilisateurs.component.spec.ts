import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetUtilisateursComponent } from './get-utilisateurs.component';

describe('GetUtilisateursComponent', () => {
  let component: GetUtilisateursComponent;
  let fixture: ComponentFixture<GetUtilisateursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetUtilisateursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetUtilisateursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
