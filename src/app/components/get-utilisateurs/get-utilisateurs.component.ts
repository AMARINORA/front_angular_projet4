import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Utilisateur } from 'src/app/Models/utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-get-utilisateurs',
  templateUrl: './get-utilisateurs.component.html',
  styleUrls: ['./get-utilisateurs.component.css']
})
export class GetUtilisateursComponent implements OnInit {

  // utilisateurs? : Utilisateur[];
  utilisateurs;
  utilisateurCourant? : Utilisateur;
  nom : '';
  prenom : '';
  mail : '';
  indexCourant: number;

  user;

  constructor(private _utilisateurService: UtilisateurService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
     this.getUtilisateurs();
  }

  getUtilisateur(id: string): void {
    this._utilisateurService.getUserById(id)
      .subscribe(data => {
          this.user = data;
          console.log(data)
        },
        error => {
          console.log(error);
        });
  }

  getUtilisateurs(): void {
    this._utilisateurService.getUsers()
      .subscribe(data => {this.utilisateurs = data;
        console.log(this.utilisateurs )
        },      
        error => { console.log(error);
        });
  }

  setActiveUtilisateur(utilisateur: Utilisateur, index: number): void {
    this.utilisateurCourant = utilisateur;
    // console.log(this.utilisateurCourant)
    this.indexCourant = index;
  }

  getUtilisateurByMail(): void {
    this._utilisateurService.getUserByMail(this.mail)
      .subscribe( data => {
          this.utilisateurs = data;
          console.log(this.utilisateurs );
        },
        error => {
          console.log(error);
        });
  }

  getUtilisateurByNomPrenom(): void {
    this._utilisateurService.getUserByNomEtPrenom(this.nom, this.prenom)
      .subscribe( data => {
          this.utilisateurs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.getUtilisateurs();
    this.utilisateurCourant = undefined;
    this.indexCourant = -1;
  }

  deleteUtilisateur(idUtilisateur): void {
    
    this._utilisateurService.deleteUser(idUtilisateur)
      .subscribe(response => {
          // console.log(response);
          console.log(this.utilisateurCourant.id)
          this.refreshList();
          this.router.navigate(['/listeUtilisateurs']);
        },
        error => {
          console.log(error);
        });
  }

}
