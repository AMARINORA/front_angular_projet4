import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilisateurService } from 'src/app/services/utilisateur.service';

@Component({
  selector: 'app-detail-utilisateur',
  templateUrl: './detail-utilisateur.component.html',
  styleUrls: ['./detail-utilisateur.component.css']
})
export class DetailUtilisateurComponent implements OnInit {

  user;
  constructor(private _utilisateurService: UtilisateurService, private route: ActivatedRoute) { }
  
  ngOnInit(): void {
    this.user = this.getUtilisateur(this.route.snapshot.params.id);
  }
  getUtilisateur(id: string): void {
    this._utilisateurService.getUserById(id)
      .subscribe(data => {
          this.user = data;
          // console.log(data)
        },
        error => {
          console.log(error);
        });
  }

}
