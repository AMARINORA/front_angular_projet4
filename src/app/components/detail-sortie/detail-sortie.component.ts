import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SortiesService } from 'src/app/services/sorties.service';

@Component({
  selector: 'app-detail-sortie',
  templateUrl: './detail-sortie.component.html',
  styleUrls: ['./detail-sortie.component.css']
})
export class DetailSortieComponent implements OnInit {

  sortie;
  access_link;
  isBlind = false;
  isDeaf = false;

  constructor(private _sortiesService: SortiesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getSortie(this.route.snapshot.params.id);
  }

  getSortie(id: string): void {
    this._sortiesService.getSortieById(id)
      .subscribe(data => {
          this.sortie = data[0];
         this.sortie.fields.description = this.sortie.fields.description.replace(/<[^>]*>?/gm, '')
         if(this.sortie.fields.blind == 1){
           this.isBlind = true
         } 
         if(this.sortie.fields.deaf == 1){
          this.isDeaf = true
        } 
          console.log(data)
        },
        error => {
          console.log(error);
        });
  }

  

}
