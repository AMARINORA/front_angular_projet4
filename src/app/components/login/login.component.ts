import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormConnexion } from 'src/app/Models/formConnexion';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formConnexion: FormConnexion = {
    mail: null,
    pwd: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService, private router: Router) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }

  onSubmit(): void {

    const data = {
      mail: this.formConnexion.mail,
      pwd: this.formConnexion.pwd
    };
     this.authService.login(this.formConnexion.mail, this.formConnexion.pwd).subscribe(
      data => {
        console.log(data)
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        // this.roles = this.tokenStorage.getUser().roles;
        // this.reloadPage();
          if (data.role == "User")
          this.router.navigate(['/dashboard-user']);
          if (data.role == "Admin")
          this.router.navigate(['/dashboard-admin']);        
      },
      err => {
        console.log(err.error.message)
        //this.errorMessage = err.error.message;
        this.errorMessage ="Email ou mot de passe incorrect"
        this.isLoginFailed = true;
      }
    );
  }

  reloadPage(): void {
    window.location.reload();
  }

}
