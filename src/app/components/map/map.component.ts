import { Component, OnInit, ViewChild } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';
import { SortiesService } from 'src/app/services/sorties.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap
  @ViewChild(MapInfoWindow, { static: false }) info: MapInfoWindow

  latitudeUtilisateur;
  longitudeUtilisateur;
  distanceM;
  distanceKm;
  messageDistanceM;
  messageDistanceKm;
  distanceK;
  marker: any = [];
  latitudes: any[];
  longitudes: any[];
  latitude;
  longitude;
  sorties: any = [];
  test;
  title;
  image;
  infoContent = '';
  markers = [];
  user;

  zoom = 13
  center: google.maps.LatLngLiteral
  options: google.maps.MapOptions = {
    zoomControl: true,
    scrollwheel: true,
    disableDoubleClickZoom: true,
    maxZoom: 30,
    minZoom: 10,
  }

  
  constructor(private _sortiesService: SortiesService) {
  }

  ngOnInit(): void {
    this.getSorties();
    console.log(this.latitudeUtilisateur)
    this.getMyPosition()

  }

  getMyPosition() {
    navigator.geolocation.getCurrentPosition((position) => {
      this.latitudeUtilisateur = position.coords.latitude
      this.longitudeUtilisateur = position.coords.longitude
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      }
      this.ajouterMarkerPositionUtilisateur()
    })
  }

  ajouterMarkerPositionUtilisateur() {
    this.marker = {
      position: {
        lat: this.latitudeUtilisateur,
        lng: this.longitudeUtilisateur,
      },
      options: {
        animation: google.maps.Animation.DROP,
        icon: {
          url: 'https://www.icone-png.com/png/48/47904.png',
          // url: 'https://www.icone-png.com/png/40/40470.png',
          scaledSize: new google.maps.Size(70, 70)
        }
      }
    }
    this.markers.push(this.marker)
  }

  getSorties(): void {
    this._sortiesService.getToutesLesSorties()
      .subscribe(data => {
        this.sorties = data;

        for (let sortie of this.sorties) {
          if (sortie.fields.lat_lon == null) {
            console.log("coordinates undefined")
          }
          else {
            this.latitude = sortie.fields.lat_lon[0]
            this.longitude = sortie.fields.lat_lon[1]
            this.title = sortie.fields.contact_name
            this.infoContent = this.infoPopupSortie(sortie)
            console.log(this.latitudeUtilisateur, this.longitudeUtilisateur)
            if (sortie.fields.cover_url == null)
              console.log("there is no image")
            this.image = sortie.fields.cover_url

            this.ajouterMarkerSortie()
          }
        }
        console.log(this.sorties)
      },
        error => {
          console.log(error);
        });
  }

  ajouterMarkerSortie() {
    this.marker = {
      position: {
        lat: this.latitude,
        lng: this.longitude,
      },
      title: this.title,
      info: this.infoContent,
      options: {
        animation: google.maps.Animation.DROP,
        icon: {
          url: this.image,
          scaledSize: new google.maps.Size(40, 30),
        }
      }
    }
    this.markers.push(this.marker)
  }

  infoPopupSortie(sortie: any,): string {
    this.infoContent =
      `<div>
  <div><img src="${sortie.fields.cover_url}" height="200" width="300"></div> 
  <br>
  <b>${sortie.fields.title}</b> 
  <br>
  <br>
  <i class="fas fa-map-pin"></i> <b>${this.calculerDistance(this.latitudeUtilisateur, this.longitudeUtilisateur, this.latitude, this.longitude)}</b>
 </div>`;
    return this.infoContent;
  }

  calculerDistance(latitude1, longitude1, latitude2, longitude2): string {
    this.distanceM = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng
      (latitude1, longitude1), new google.maps.LatLng(latitude2, longitude2));
      this.distanceM = Math.trunc(this.distanceM)
      this.distanceK = this.distanceM/1000
      this.distanceK = this.distanceK.toFixed(2)
      if(this.distanceK < 1) {
        return this.distanceM + " m de chez vous"
      } else {
        return this.distanceK +" km de chez vous"
      }
  }

  zoomIn() {
    if (this.zoom < this.options.maxZoom) this.zoom++
  }

  zoomOut() {
    if (this.zoom > this.options.minZoom) this.zoom--
  }

  click(event: google.maps.MouseEvent) {
    console.log(event)
  }

  openInfo(marker: MapMarker, content) {
    this.infoContent = content
    this.info.open(marker)
  }




}
