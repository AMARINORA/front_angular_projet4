import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FavoriService } from 'src/app/services/favori.service';
import { SortiesService } from 'src/app/services/sorties.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-favori',
  templateUrl: './favori.component.html',
  styleUrls: ['./favori.component.css']
})
export class FavoriComponent implements OnInit {

  favoris;
  sortiesFavorites: any = [];
  sortieFavorite;
  isListFavoris = true;
  user;
  tabTest;

  constructor(private tokenStorageService: TokenStorageService, private _favoriService: FavoriService,
     private _sortiesService: SortiesService, private router: Router ) { }

  ngOnInit(): void {
    this.getMesFavoris()
  }

  getMesFavoris(): void {
    const dataUser = {
      utilisateur: this.tokenStorageService.getUser()
    };
    this._favoriService.getMesFavoris(dataUser.utilisateur.id)
      .subscribe(data => {
        this.favoris = data;

        for(let favori of this.favoris){
        this._sortiesService.getSortieById(favori.idSortie).subscribe(data => {
         this.tabTest = data;
          console.log(this.tabTest.length)
           if (this.tabTest.length != 0)
             this.sortiesFavorites.push(data[0])
          });
        }
        // console.log(this.sortiesFavorites)
        // if (this.sortiesFavorites.length == 0){
        //   console.log(this.sortiesFavorites.length)
        //    this.isListFavoris = false;
        //    console.log(this.isListFavoris)
        // }        
      },     
        error => {
          console.log(error);
        });
  }

  refreshListFavoris(): void {
    while (this.sortiesFavorites.length) {
      this.sortiesFavorites.pop();
    }
    this.getMesFavoris()
  }

  retirerFavori(idSortie: any): void {
    this.user = this.tokenStorageService.getUser()

    this._favoriService.deleteFavori(idSortie,this.user.id)
      .subscribe(response => {
        this.refreshListFavoris()
          console.log(response); 
        },
        error => {
          console.log(error);
        });
  }


}
