import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  user;
  isAdmin: boolean;

  constructor(private tokenStorageService: TokenStorageService ) { }

  ngOnInit(): void {
    this.user = this.tokenStorageService.getUser()
    if(this.user.role == "Admin"){
      this.isAdmin = true;
    }else {
      this.isAdmin = false;
    }

  }

}
