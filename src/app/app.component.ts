import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'front-angular';

  isLoggedIn = false;
  roles: any []
  showAdminBoard
  showUserBoard
  email
  isAdmin = false;
  isUser=false
  constructor(private tokenStorageService: TokenStorageService){}

  ngOnInit(): void{ 
    console.log("app.module")


    this.tokenStorageService.estConnecte.subscribe(isConnect => {
      this.isLoggedIn = !!this.tokenStorageService.getToken();
      console.log(this.isLoggedIn)
      console.log(isConnect)
      if(isConnect){
        const user = this.tokenStorageService.getUser();
        if(user.role == "Admin"){
          this.isAdmin = true;
          console.log(this.isAdmin)
          console.log(this.isUser)
        } else if(user.role =="User") {
          this.isAdmin = false;
          this.isUser = true;
          console.log(this.isAdmin)
          console.log(this.isUser)
        }
        // this.roles = user.role;
        //   this.showAdminBoard = this.roles.includes('Admin');
        //   this.showUserBoard = this.roles.includes('User');
        this.email = user.mail;
      }
    });
  
  }

}

  

 


