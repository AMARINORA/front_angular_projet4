import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './components/accueil/accueil.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { BordUserComponent } from './components/bord-user/bord-user.component';
import { DetailSortieComponent } from './components/detail-sortie/detail-sortie.component';
import { DetailUtilisateurComponent } from './components/detail-utilisateur/detail-utilisateur.component';
import { FavoriComponent } from './components/favori/favori.component';
import { FormInscriptionComponent } from './components/form-inscription/form-inscription.component';
import { GetUtilisateursComponent } from './components/get-utilisateurs/get-utilisateurs.component';
import { LoginComponent } from './components/login/login.component';
import { MapComponent } from './components/map/map.component';
import { RechercheComponent } from './components/recherche/recherche.component';
import { SortiesComponent } from './components/sorties/sorties.component';
import { UpdateUtilisateurComponent } from './components/update-utilisateur/update-utilisateur.component';
import { MapAdresseComponent } from './map-adresse/map-adresse.component';

const routes: Routes = [
  // {path: '', pathMatch: 'full', redirectTo: ''}, 
  {path: '', component: AccueilComponent},
  {path: 'routesorties', component: SortiesComponent},
  {path: 'modification/:id', component: UpdateUtilisateurComponent},
  {path: 'listeUtilisateurs', component: GetUtilisateursComponent},
  {path: 'inscription', component: FormInscriptionComponent},
  {path: 'login', component: LoginComponent},
  {path: 'dashboard-user', component: BordUserComponent},
  {path: 'dashboard-admin', component: BoardAdminComponent},
  {path: 'map', component: MapComponent},
  {path: 'mapAdresse', component: MapAdresseComponent},
  {path: 'rechercheSorties', component: RechercheComponent},
  {path: 'details/:id', component: DetailSortieComponent},
  {path: 'mesFavoris', component: FavoriComponent},
  {path: 'detailsUtilisateur/:id', component: DetailUtilisateurComponent},
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
