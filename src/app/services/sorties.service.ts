import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SortiesService {

  // ROOT_URL: string = 'http://localhost:3000';
  ROOT_URL: string = environment.PROJET4_AL7_API;

  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }

  getToutesLesSorties() {
    var uri = "sorties/toutesLesSorties";
    return this.httpClient.get(`${this.ROOT_URL}/${uri}`);
  }

  getSortieById(id: string) : Observable<Object>{
    var uri = "sorties/chercherSortie";
    return this.httpClient.get(`${this.ROOT_URL}/${uri}/${id}`);
  }

  getSortiesByArrdt(arrdt: any): Observable<Object[]>{
    var uri = "sorties/arrondissement";
    return this.httpClient.get<Object[]>(`${this.ROOT_URL}/${uri}/${arrdt}`);
  }

  getSortiesByCategorie(categorie: any): Observable<Object[]>{
    var uri = "sorties/categorie";
    return this.httpClient.get<Object[]>(`${this.ROOT_URL}/${uri}/${categorie}`);
  }

  getSortiesByPrixType(prixType: any): Observable<Object[]>{
    var uri = "sorties/prix";
    return this.httpClient.get<Object[]>(`${this.ROOT_URL}/${uri}/${prixType}`);
  }

  getSortiesForBlind(): Observable<Object[]>{
    var uri = "sorties/blind/1";
    return this.httpClient.get<Object[]>(`${this.ROOT_URL}/${uri}`);
  }

  getSortiesForDeaf(): Observable<Object[]>{
    var uri = "sorties/deaf/1";
    return this.httpClient.get<Object[]>(`${this.ROOT_URL}/${uri}`);
  }


  





}
