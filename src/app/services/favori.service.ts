import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FavoriService {

  ROOT_URL: string = environment.PROJET4_AL7_API_JAVA;

  httpHeaders = new HttpHeaders()
   .set('content-type','application/json')
   .set('Access-Control-Allow-Origin', '*')

   constructor(private httpClient: HttpClient) { }

   AddFavori(data: any): Observable<any> {
    var uri = "utilisateur/ajouterFavori";
    let API_URL = `${this.ROOT_URL}/${uri}`;
    return this.httpClient.post(API_URL, data)
      .pipe(
        catchError(this.handleError)
      )
  }

  getMesFavoris(id: string) : Observable<Object[]>{
    var uri = "utilisateur/mesFavoris";
    return this.httpClient.get<Object[]>(`${this.ROOT_URL}/${uri}/${id}`);
  }

  chercherFavori(data: any) : Observable<Object[]>{
    var uri = "rechercherFavoriIdSortieEtIdUtilisateur";
    return this.httpClient.get<Object[]>(`${this.ROOT_URL}/${uri}/${data}`);
  }
  
  deleteFavori(idSortie: any,idUser: any) {
    var uri = 'utilisateur/retirerFavori'
    return this.httpClient.post(`${this.ROOT_URL}/${uri}`,{
      idSortie,idUser
    })
  }



  // Error 
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
