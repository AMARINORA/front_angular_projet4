import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

  const API_URL: string = environment.PROJET4_AL7_API_JAVA;
  const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(mail: string, pwd: string): Observable<any> {
    var uri = "utilisateur/connecterUtilisateur"
    return this.http.post(API_URL + `/${uri}`, {
      mail,
      pwd
    }, httpOptions);
  }
}
