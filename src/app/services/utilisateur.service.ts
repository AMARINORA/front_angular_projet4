import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Utilisateur } from '../Models/utilisateur';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  // ROOT_URL: string = 'https://projet4-spring-java-staging.herokuapp.com/';
   ROOT_URL: string = environment.PROJET4_AL7_API_JAVA;
  
  httpHeaders = new HttpHeaders()
   .set('content-type','application/json')
   .set('Access-Control-Allow-Origin', '*')

  constructor(private httpClient: HttpClient) { }

  AddUser(data: any): Observable<any> {
    var uri = "utilisateur/ajouterUtilisateur";
    let API_URL = `${this.ROOT_URL}/${uri}`;
    return this.httpClient.post(API_URL, data)
      .pipe(
        catchError(this.handleError)
      )
  }

   // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type':  'application/json',
    //     Authorization: 'my-auth-token',
    //     'Access-Control-Allow-Origin': '*'
    //   })
    // };

  getUsers() : Observable<Utilisateur[]>{
    var uri = "utilisateur/listerLesUtilisateurs";
    return this.httpClient.get<Utilisateur[]>(`${this.ROOT_URL}/${uri}`);
  }

  getUserById(id: string): Observable<Object> {
    var uri = "utilisateur/rechercherUtilisateurId"
    return this.httpClient.get(`${this.ROOT_URL}/${uri}/${id}`);
  }

  getUserByMail(mail: string): Observable<Utilisateur> {
    var uri = "utilisateur/rechercherUtilisateurMail"
    return this.httpClient.get<Utilisateur>(`${this.ROOT_URL}/${uri}/${mail}`);
  }

  getUserByNomEtPrenom(nom: any, prenom: any): Observable<Utilisateur[]> {
    var uri = "utilisateur/rechercherUtilisateurNomPrenom/"+nom+"/"+prenom;
    return this.httpClient.get<Utilisateur[]>(`${this.ROOT_URL}/${uri}/${uri}`);
  }

  updateUser(id: any, data: any): Observable<any> {
    var uri = "utilisateur/modifierUtilisateur" ;
    return this.httpClient.put(`${this.ROOT_URL}/${uri}/${id}`, data);
  }

  updatePwdUser(id: any, data: any): Observable<any> {
    var uri = "utilisateur/modifierMdpUtilisateur" ;
    return this.httpClient.put(`${this.ROOT_URL}/${uri}/${id}`, data);
  }

  deleteUser(id: string): Observable<Utilisateur> {
    var uri = 'utilisateur/supprimerUtilisateur'
    return this.httpClient.delete<Utilisateur>(`${this.ROOT_URL}/${uri}/${id}`);
  }

  getIsFavoris(idSortie: any,idUtilisateur:any): Observable<Object[]>{
    var uri = "utilisateur/isFavori/"+idSortie+"/"+idUtilisateur;
    return this.httpClient.get<Object[]>(`${this.ROOT_URL}/${uri}`);
  }


  // Error 
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
