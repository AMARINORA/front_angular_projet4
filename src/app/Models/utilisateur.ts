import { Adresse } from "./adresse";

export class Utilisateur {
  id? : string;
  nom?: string;
  prenom?: string;
  adresse: Adresse;
  mail? : string;
  pwd? : string;
}
