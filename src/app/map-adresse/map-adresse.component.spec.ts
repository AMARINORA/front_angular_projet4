import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapAdresseComponent } from './map-adresse.component';

describe('MapAdresseComponent', () => {
  let component: MapAdresseComponent;
  let fixture: ComponentFixture<MapAdresseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapAdresseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapAdresseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
