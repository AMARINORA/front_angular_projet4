import { Component, OnInit, ViewChild } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';
import { SortiesService } from 'src/app/services/sorties.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-map-adresse',
  templateUrl: './map-adresse.component.html',
  styleUrls: ['./map-adresse.component.css']
})
export class MapAdresseComponent implements OnInit {
  @ViewChild(GoogleMap, { static: false }) map: GoogleMap
  @ViewChild(MapInfoWindow, { static: false }) info: MapInfoWindow

  zoom = 13
  center;
  options: google.maps.MapOptions = {
    zoomControl: true,
    scrollwheel: true,
    disableDoubleClickZoom: true,
    maxZoom: 30,
    minZoom: 10,
  }

  marker: any = [];
  latitudes: any[];
  longitudes: any[];
  latitude;
  longitude;

  sorties: any = [];
  test;

  title;
  image;

  markers = []
  user;

  infoContent = ''
  distanceM;
  latitudeUser: number;
  longitudeUser: number;
  distanceK;

  constructor(private tokenStorageService : TokenStorageService, private _sortiesService: SortiesService) { }

  ngOnInit(): void {
    this.user = this.tokenStorageService.getUser();
    this.latitude = parseFloat(this.user.adresse.latitude)
    this.longitude = parseFloat(this.user.adresse.longitude)
    this.center = {
      lat: this.latitude,
      lng: this.longitude,
    }
    this.ajouterMarkerDomicile()
    this.getSorties()
  }

  ajouterMarkerDomicile(){
    this.marker = {
      position: {
        lat: this.latitude,
        lng: this.longitude,
      },
      info:  this.infoPopupDomicile(),
      options: {
        animation: google.maps.Animation.DROP,
        icon: {
          url: 'https://www.icone-png.com/png/31/31281.png',
          scaledSize: new google.maps.Size(70, 70)
        },
      }
    }
    this.markers.push(this.marker)
  }

  getSorties(): void {
    this._sortiesService.getToutesLesSorties()
      .subscribe(data => {
        this.sorties = data;
        console.log(this.sorties)
        for (let sortie of this.sorties) {
          if (sortie.fields.lat_lon == null) {
            console.log("coordinates undefined")
          }
          else {
            this.latitude = sortie.fields.lat_lon[0]
            this.longitude = sortie.fields.lat_lon[1]
            this.title = sortie.fields.title
            this.image = sortie.fields.cover_url 

            this.user = this.tokenStorageService.getUser();
            this.latitudeUser = parseFloat(this.user.adresse.latitude)
            this.longitudeUser = parseFloat(this.user.adresse.longitude)

            this.infoContent = this.infoPopupSortie(sortie)
            // this.calculerDistance(this.latitudeUser, this.longitudeUser, this.latitude, this.longitude)
            if (sortie.fields.cover_url == null)
              console.log("there is no image")                     
            this.ajouterMarkerSortie()
          }
        }
      },
        error => {
          console.log(error);
        });
  }

  infoPopupSortie(sortie: any,): string {
    this.infoContent =
      `<div>
  <div><img src="${sortie.fields.cover_url}" height="200" width="320"></div> 
  <br>
  <b>${sortie.fields.title}</b> 
  <br>
  <br>
  <i class="fas fa-map-pin"></i> <b>${this.calculerDistance(this.latitudeUser, this.longitudeUser, this.latitude, this.longitude)}</b>
 </div>`;
    return this.infoContent;
  }

  infoPopupDomicile(): string {
    this.infoContent =
      `<div>
  <div><img src="https://www.icone-png.com/png/31/31281.png" height="100" width="100"></div> 
  <br>
  <b>Home Sweet Home</b> 
 </div>`;
    return this.infoContent;
  }

  ajouterMarkerSortie() {
    this.marker = {
      position: {
        lat: this.latitude,
        lng: this.longitude,
      },
      title: this.title,
      info: this.infoContent,
      options: {
        animation: google.maps.Animation.DROP,
        icon: {
          url: this.image,
          scaledSize: new google.maps.Size(40, 30),
        }
      }
    }
    this.markers.push(this.marker)
  }

  calculerDistance(latitude1, longitude1, latitude2, longitude2): string {
    this.distanceM = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng
      (latitude1, longitude1), new google.maps.LatLng(latitude2, longitude2));
      this.distanceM = Math.trunc(this.distanceM)
      this.distanceK = this.distanceM/1000
      this.distanceK = this.distanceK.toFixed(2)
      if(this.distanceK < 1) {
        return this.distanceM + " m de chez vous"
      } else {
        return this.distanceK +" km de chez vous"
      }
      
  }

  zoomIn() {
    if (this.zoom < this.options.maxZoom) this.zoom++
  }

  zoomOut() {
    if (this.zoom > this.options.minZoom) this.zoom--
  }

  click(event: google.maps.MouseEvent) {
    console.log(event)
  }

  openInfo(marker: MapMarker, content) {
    this.infoContent = content
    this.info.open(marker)
  }

}
