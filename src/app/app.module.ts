import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormInscriptionComponent } from './components/form-inscription/form-inscription.component';
import { GetUtilisateursComponent } from './components/get-utilisateurs/get-utilisateurs.component';
import { UpdateUtilisateurComponent } from './components/update-utilisateur/update-utilisateur.component';
import { SortiesComponent } from './components/sorties/sorties.component';
import { MapComponent } from './components/map/map.component';
import { LoginComponent } from './components/login/login.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { BordUserComponent } from './components/bord-user/bord-user.component';

import { GoogleMapsModule } from '@angular/google-maps';

import { AccueilComponent } from './components/accueil/accueil.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RechercheComponent } from './components/recherche/recherche.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CustomDatePipe } from './Date/custom.datepipe';
import { FavoriComponent } from './components/favori/favori.component';
import { DetailSortieComponent } from './components/detail-sortie/detail-sortie.component';
import { MapAdresseComponent } from './map-adresse/map-adresse.component';
import { DetailUtilisateurComponent } from './components/detail-utilisateur/detail-utilisateur.component';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FormInscriptionComponent,
    GetUtilisateursComponent,
    UpdateUtilisateurComponent,
    SortiesComponent,
    MapComponent,
    LoginComponent,
    BoardAdminComponent,
    BordUserComponent,
    MapAdresseComponent,
    AccueilComponent,
    RechercheComponent,
    CustomDatePipe,
    FavoriComponent,
    DetailSortieComponent,
    DetailUtilisateurComponent,
  ],
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule, HttpClientModule, 
    AppRoutingModule, GoogleMapsModule, MDBBootstrapModule.forRoot(), AccordionModule.forRoot()
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
